﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private Text recordText;
    [SerializeField] private Text appleText;
    public void OpenScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Update()
    {
        recordText.text = PlayerPrefs.GetInt("Record").ToString();
        appleText.text = PlayerPrefs.GetInt("Apple").ToString();
    }

    public void DelKeys()
    {
        PlayerPrefs.DeleteAll();
    }
}
