﻿using UnityEngine;

public class AppleSpawner : MonoBehaviour
{
    [SerializeField] private Transform spawnPos;
    [SerializeField] private GameObject apple;
    [SerializeField] private int spawnChance = 25;
    void Start()
    {
        float Range = Random.Range(0f, 101f);
        if (spawnChance > Range)
        {
            Instantiate(apple, spawnPos);
        }
    }
}
